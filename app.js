let imgi = document.getElementById("main-img");

// Dropzone.autoDiscover = false;

// Dropzone.options.myDropzone = {
//   url: "/",
//   thumbnailWidth: null,
//   thumbnailHeight: null,
//   init: function () {
//     this.on("thumbnail", function (file, dataUrl) {
//       $(".dz-image").last().find("img").attr({ width: "100%", height: "100%" });
//     }),
//       this.on("success", function (file) {
//         $(".dz-image").css({ width: "100%", height: "auto" });
//       });
//   },
// };

// var myDropzone = new Dropzone("div#myDropzone");

var dropzone = new Dropzone("#demo-upload", {
  // previewTemplate: document.querySelector("#preview-template").innerHTML,
  parallelUploads: 2,
  thumbnailHeight: 180,
  thumbnailWidth: 180,
  maxFilesize: 6,
  filesizeBase: 1000,
  thumbnail: function (file, dataUrl) {
    if (file.previewElement) {
      file.previewElement.classList.remove("dz-file-preview");
      var images = file.previewElement.querySelectorAll("[data-dz-thumbnail]");
      for (var i = 0; i < images.length; i++) {
        var thumbnailElement = images[i];
        thumbnailElement.alt = file.name;
        thumbnailElement.src = dataUrl;
      }
      setTimeout(function () {
        file.previewElement.classList.add("dz-image-preview");
      }, 1);
    }
  },
});

function submit() {
  var node = document.querySelector(".dz-image").getElementsByTagName("img")[0];
  console.log(node);
  imgi.src = node.src;
}

function download() {
  console.log("in");
  const scale = 2; // quality of image
  const node = document.querySelector(".download-img");
  const style = {
    transform: "scale(" + scale + ")",
    transformOrigin: "top left",
    width: node.offsetWidth + "px",
    height: node.offsetHeight + "px",
  };

  const param = {
    height: node.offsetHeight * scale,
    width: node.offsetWidth * scale,
    quality: 1,
    style,
  };

  domtoimage.toBlob(node, param).then(function (blob) {
    window.saveAs(blob, "aap-dyk.png");
  });
}
